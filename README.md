# Chemometrics applications for multivariate calibration

Applications of different chemometrics techniques for multivariate calibration


1) Exhaustive preprocessing search for PLSR in [multivarcal_preprocessing](https://gitlab.com/chemsoftware/python/chemdemos/-/tree/main/multivarcal_preprocessing)

2) Exhaustive search of interval variable selection for PLSR in [multivarcal_varselection](https://gitlab.com/chemsoftware/python/chemdemos/-/tree/main/multivarcal_varselection)

3) Classical calibration transfer in [caltr_stdmethods](https://gitlab.com/chemsoftware/python/chemdemos/-/tree/main/caltr_stdmethods)



# Contact

valeria.fonseca.diaz@gmail.com

valeria.fonsecadiaz@kuleuven.be

https://vfonsecad.gitlab.io/connect/author/vfonsecad/


