# ------------------------------------------------------------------------
#                           preprocessing tune functions
# by: Valeria Fonseca Diaz
#  ------------------------------------------------------------------------


import numpy as np
from modules.preprocessing import preprocessing


def tune_osc(xcal_dict, ycal,xtest_dict, total_osc_range = np.arange(1,2), osc_lv_range = np.arange(1,31)):
    
    '''
    tune osc preprocessing from preprocessing module for a range of OSC components and LV to fit the OSC components and apply to entries in xcal_dict  and xtest_dict
    
    total_osc_range: np.arange(1,2)
    osc_lv_range: np.arange(1,31)
    
    xcal_dict: dictionary with versions of xcal. Basic dictionary is {"original": xcal}
    xtest_dict: dictionary with versions of xtest. Basic dictionary is {"original": xtest}
    
    output: (xcal_osc_dict, xtest_osc_dict)
    
    '''


    xcal_osc_dict = {}
    xtest_osc_dict = {}
    
    
    for ii_key0 in xcal_dict.keys():
        
        my_pp = preprocessing(xcal_dict[ii_key0], ycal.copy())

        for total_osc in total_osc_range:
            
            for osc_lv in osc_lv_range:

                osc_fit = my_pp.osc(current_osc_ncp = total_osc,osc_pls_lv = osc_lv) 
                ii_key = ii_key0 + " + osc comp: " + str(total_osc)+ " - osc_pls lv: " + str(osc_lv)
                xcal_osc_dict[ii_key] = osc_fit[0].copy()
                xtest_osc_dict[ii_key] = my_pp.apply_osc(xtest_dict[ii_key0], osc_fit[1])
            
    return (xcal_osc_dict, xtest_osc_dict)



def tune_savgol_derivative(xcal_dict, xtest_dict, max_polynomial_order = 3, min_derivative_order = 2,min_ww = 3, max_ww = 21,increase_step_ww = 5):
    
    '''
    tune derivatives savgol parameters and apply to entries in xcal_dict  and xtest_dict
        
    xcal_dict: dictionary with versions of xcal. Basic dictionary is {"original": xcal}
    xtest_dict: dictionary with versions of xtest. Basic dictionary is {"original": xtest}
    max_polynomial_order: int. Maximum polynomial order
    min_derivative_order: int. Minimum derivative order
    min_ww: odd interger min value of the window width. 
    max_ww: odd interger max value of the window width. 
    increase_step_ww: int. step of increasing the size of the window_width    

 
    output: (xcal_deriv_dict, xtest_deriv_dict)    
    '''
    
    assert max_polynomial_order < increase_step_ww, "max_polynomial_order must be lower than increase_step_ww"
    

    xcal_deriv_dict = {}
    xtest_deriv_dict = {}

    for ii_key0 in xcal_dict.keys():

        my_pp = preprocessing(xcal_dict[ii_key0])

        for polynomial_order in np.arange(min_derivative_order+1, max_polynomial_order+1):

            for derivative_order in np.arange(min_derivative_order,polynomial_order):

                min_ww_final = np.amax([min_ww, polynomial_order+1])
                max_ww_final = np.amin([max_ww, xcal_dict[ii_key0].shape[1]-10])
                
                for window_width in np.arange(min_ww_final, max_ww_final,increase_step_ww):

                    if (window_width%2)==0:
                        window_width = window_width+1


                    ii_key = ii_key0 + " + deriv ww:  " + str(window_width)+ " - deriv order: " + str(derivative_order) + " - poly order: " + str(polynomial_order)

                    xcal_deriv_dict[ii_key] = my_pp.savgol_derivative(window_width = window_width, polynomial_order = polynomial_order, derivative_order = derivative_order)
                    xtest_deriv_dict[ii_key] = my_pp.savgol_derivative(window_width = window_width, polynomial_order = polynomial_order, derivative_order = derivative_order,xx_test=xtest_dict[ii_key0])


    return (xcal_deriv_dict, xtest_deriv_dict)


def tune_msc(xcal_dict,xtest_dict, xmean = True, xmedian=True, xref_matrix=None):
    
    '''
    tune reference spectrum for msc and apply to entries in xcal_dict  and xtest_dict
        
    xcal_dict: dictionary with versions of xcal. Basic dictionary is {"original": xcal}
    xtest_dict: dictionary with versions of xtest. Basic dictionary is {"original": xtest}
    xmean: boolean. MSC with mean reference
    xmedian: boolean. MSC with median reference
    xref_matrix: Nref x K matrix of other possible references. K is the same dimensionality as xcal and xtest
 
    output: (xcal_msc_dict,xtest_msc_dict)
    '''
    

    xcal_msc_dict = {}
    xtest_msc_dict = {}
    
    
    for ii_key0 in xcal_dict.keys():
    
        my_pp = preprocessing(xcal_dict[ii_key0])

        if xmean:

            msc_fit = my_pp.msc(x_ref0 = np.mean(xcal_dict[ii_key0],axis=0))
            xcal_msc_dict[ii_key0 + " + msc mean"] = msc_fit[0]
            xtest_msc_dict[ii_key0 + " + msc mean"] = my_pp.msc(x_ref0 = msc_fit[1].flatten(),xx_test=xtest_dict[ii_key0])[0]

        if xmedian:

            msc_fit = my_pp.msc(x_ref0 = np.median(xcal_dict[ii_key0],axis=0))
            xcal_msc_dict[ii_key0 + " + msc median"] = msc_fit[0]
            xtest_msc_dict[ii_key0 + " + msc median"] = my_pp.msc(x_ref0 = msc_fit[1].flatten(),xx_test=xtest_dict[ii_key0])[0]

        if xref_matrix is not None:

            for ii in np.arange(xref_matrix.shape[0]):

                msc_fit = my_pp.msc(x_ref0 = xref_matrix[ii,:])
                xcal_msc_dict[ii_key0 + " + msc " + str(ii)] = msc_fit[0]
                xtest_msc_dict[ii_key0 + " + msc " + str(ii)] = my_pp.msc(x_ref0 = msc_fit[1].flatten(),xx_test=xtest_dict[ii_key0])[0]




    return (xcal_msc_dict,xtest_msc_dict)



def tune_snv(xcal_dict,xtest_dict):
    
    '''
    apply snv to entries in xcal_dict  and xtest_dict
        
    xcal_dict: dictionary with versions of xcal. Basic dictionary is {"original": xcal}
    xtest_dict: dictionary with versions of xtest. Basic dictionary is {"original": xtest}
   
 
    output: (xcal_snv_dict,xtest_snv_dict)
    '''
    
    xcal_snv_dict = {}
    xtest_snv_dict = {}

    for ii_key0 in xcal_dict.keys():

        my_pp = preprocessing(xcal_dict[ii_key0])   

        xcal_snv = my_pp.snv()
        
        xcal_snv_dict[ii_key0 + " + snv"] = xcal_snv.copy()
        xtest_snv_dict[ii_key0 + " + snv"] = my_pp.snv(xx_test = xtest_dict[ii_key0])
        
    return (xcal_snv_dict,xtest_snv_dict)
