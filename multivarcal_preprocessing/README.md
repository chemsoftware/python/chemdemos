# Preprocessing search for multivariate calibration building

Exhaustive application and training of preprocessing techniques


- Orthogonal Signal Correction (OSC)
- Derivatives
- Multiplicative Scattering Correction (MSC)
- Standard Normal Variate (SNV)

# Local modules

```python

# preprocessing class with techniques
from modules.preprocessing import preprocessing  
# preprocessing functions for exhaustive search and tuning
import modules.preprocessing_tune as tune_pp

```

# Performance of resulting best model(s)


![1](figures/spectra.png)
![2](figures/rmsep.png)
![3](figures/r2p.png)



# Acknowledgements
As part of our doctoral research, we disseminate the available methodology for the chemometrics community at [MeBioS Biophotonics](https://www.biw.kuleuven.be/biosyst/mebios/biophotonics-group/biophotonics-staff)

Ph.D. research funded by [The Research Foundation – Flanders](https://www.fwo.be/) in Belgium


# References

Helland, I. S., Naes, T., and Isaksson, T. (1995). Related versions of the multiplicative scatter correction
method for preprocessing spectroscopic data. Chemometrics and Intelligent Laboratory Systems,
29(2):233–241

Barnes, R., Dhanoa, M., and Lister, S. (1989). Standard Normal Variate Transformation and De-trending
of Near-Infrared Diffuse Reflectance Spectra. Appl. Spectrosc., 43(5):772–777.


Savitzky, A. and Golay, M. J. E. (1964). Smoothing and Differentiation of Data by Simplified Least
Squares Procedures. Analytical Chemistry, 36(8):1627–1639.


Wold, S., Antti, H., Lindgren, F., and Ohman, J. (1998). Orthogonal signal correction of near-infrared ¨
spectra. Chemometrics and Intelligent Laboratory Systems, 44(1-2):175–185.