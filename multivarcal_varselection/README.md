# Variable selection with interval search for PLS

Exhaustive search of variable selection with interval search


# Local modules

```python

# ipls definition
import modules.variable_selection as varsel

```

# Selected intervals


![1](figures/intervals.svg)



# Acknowledgements
As part of our doctoral research, we disseminate the available methodology for the chemometrics community at [MeBioS Biophotonics](https://www.biw.kuleuven.be/biosyst/mebios/biophotonics-group/biophotonics-staff)

Ph.D. research funded by [The Research Foundation – Flanders](https://www.fwo.be/) in Belgium


# References

L. Nergaard, A. Saudland, J. Wagner, J.P. Nielsen, L. Munck, and S.B. Engelsen, “Interval Partial Least-Squares Regression (iPLS): A Comparative Chemometric Study with an Example from Near-Infrared Spectroscopy” Applied Spectroscopy, 54 (3), 413-419, 2000

