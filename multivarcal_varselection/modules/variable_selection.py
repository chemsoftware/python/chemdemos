
# by: Valeria Fonseca Diaz



import numpy as np
from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, r2_score



def plsr_univariate_cv(xx,yy,lv):
    
    parameters = {'n_components': [lv]}
    pls_scoring = {'r2': 'r2', 'rmse': 'neg_root_mean_squared_error'}
    pls_cv = KFold(n_splits=10, shuffle=False)
    my_pls = PLSRegression(scale=False)
    my_pls_cv = GridSearchCV(my_pls, parameters, cv = pls_cv, scoring = pls_scoring, refit = False)
    my_pls_cv.fit(xx, yy)
    rmsecv = -1*my_pls_cv.cv_results_["mean_test_rmse"]
    return rmsecv[0]





def iplsr_univariate(xx, yy, chosen_lv,interval_size, forward = True, verbose = True):

    N = yy.shape[0]
    kk2 = xx.shape[1]

    
    # --- variable selection

        # --- intervals id

    wv_init = 0
    ww = int(interval_size/2)
    intervals_id = np.zeros(kk2, dtype=np.int64)


    if (wv_init-ww) >= 0:
        wv_init_ll = wv_init-ww
    else:
        wv_init_ll = 0
        
    if (wv_init+ww+1) <= kk2:
        wv_init_ul = wv_init+ww+1
    else:
        wv_init_ul = kk2
        
      
    intervals_id[wv_init_ll:wv_init_ul] = 1

    current_wv = wv_init_ul
    ss = 2
    while current_wv < kk2:
        intervals_id[current_wv:(current_wv+interval_size)] = ss
        current_wv += interval_size
        ss += 1

    current_wv = 0
    while current_wv < wv_init_ll-interval_size:
        intervals_id[current_wv:(current_wv+interval_size)] = ss
        current_wv += interval_size
        ss += 1
    intervals_id[intervals_id==0] = ss


    unique_intervals = np.sort(np.unique(intervals_id))

        # --- loop interval selection

    nonstop = True
    intervals_inside = np.zeros(unique_intervals.shape[0], dtype=np.int64)

    current_rmsecv_final = 10*yy.std()
    selected_ii = 0
    itt = 0

    while itt<=2:

        current_rmsecv = current_rmsecv_final
        if verbose:
            print(current_rmsecv_final)

        for ii in unique_intervals[intervals_inside==0]:
            
            

            current_intervals = np.append(intervals_inside[intervals_inside!=0], ii)
            current_intervals_bool = np.zeros(kk2, dtype=np.int64) == 1

            for jj in range(kk2):
                if np.sum(current_intervals == intervals_id[jj])>0:
                    current_intervals_bool[jj] = True

            
            
            if chosen_lv > np.sum(current_intervals_bool):
                chosen_lv_cv = np.sum(current_intervals_bool)
            else:
                chosen_lv_cv = chosen_lv
            
            current_rmsecv_iter = plsr_univariate_cv(xx[:,current_intervals_bool==forward], yy, chosen_lv_cv)

            
            if current_rmsecv_iter <= current_rmsecv:
                current_rmsecv = current_rmsecv_iter
                selected_ii = ii
               

        intervals_inside[unique_intervals==selected_ii] = selected_ii

        if current_rmsecv >= current_rmsecv_final or itt >= unique_intervals[-1]:
            nonstop = False
        itt += 1
        current_rmsecv_final = current_rmsecv
      
    if verbose:
        print(current_rmsecv_final)
    
    # --- final intervals
    
    intervals_inside_bool = np.zeros(kk2, dtype=np.int64) == 1
    for jj in range(kk2):
        if np.sum(intervals_inside[intervals_inside!=0]==intervals_id[jj])>0:
            intervals_inside_bool[jj] = True
    

   
    return intervals_inside_bool==forward