
import numpy as np


def genstruct(nstruct, nvar, ngauss, random_seed = 2394875):
    
    '''
    Function to generate structures such as loadings P using a mixture of gaussians
    
    Parameters:
    -----------
    
    nstruct: number of loading vectors
    nvar: number of spectral variables
    ngauss: number of gaussians for the mixture
    random_seed: int. Seed to use in the simulation
    
    Returns:
    --------
    
    structures: float array of shape (nstruct,nvar)
    
    '''

    structures = np.zeros((nstruct,nvar))

    z = np.arange(nvar)

    for a in range(nstruct):
        
        rng = np.random.RandomState(random_seed*a)
       
        x = rng.choice(nvar, ngauss, replace = True)    
        sd = rng.choice(np.arange(10,200), ngauss, replace = True)
        vec = np.zeros(nvar)

        for i in range(ngauss):

            gaussdens = (1/(np.sqrt(2*np.pi)*sd[i]))*np.exp(-(0.5/np.power(sd[i],2))*np.power(z-x[i],2))
            vec += gaussdens

        structures[a,:] = vec[:]
        
    structures_svd = np.linalg.svd(structures)
        
    return structures,structures_svd[2][0:nstruct,:]


