# ---------------------------

# class unsupervised sample selection
# by: Valeria Fonseca Diaz
# ---------------------------






import numpy as np
from scipy.spatial import distance
from sklearn.metrics import pairwise_distances




class sample_selection(object):

    def __init__(self, xx, yy=np.empty([0,0]), ncp = 10):

        ''' Initialize a Sample selection Class object with provided spectral data and possible reference values (optional)
        For some of the sample selection strategies a dimension reduction is needed, especially for cases where K (#vars) >> N (# samples)
        
        --- Input ---
        xx: Spectral matrix of N rows and K columns
        yy: optional. Reference values, matrix of N rows and YK columns
        ncp: number of components for PCA dimension reduction, Default ncp=10
        
        '''

        assert type(xx) is np.ndarray and type(yy) is np.ndarray and xx.shape[0] >= yy.shape[0]
        
        self.xcal = xx.copy()
        self.ycal = yy.copy()        
        self.Ncal = xx.shape[0]
        self.XK = xx.shape[1]
        self.YK = yy.shape[1]
        self.ncp = ncp
        
    
        
              
    def __str__(self):
        return 'sample selection class'

    # --- Copy of data

    def get_xcal(self):
        ''' Get copy of xcal data '''
        return self.xcal

    def get_ycal(self):
        ''' Get copy of ycal data '''
        return self.ycal
    
    # ----------------------------- pca reduction ------------------------------------
    
    
    def get_xcal_pca_scores(self):
        
        '''
        Dimension reduction based on X = USV' with X centered
        
   
        --- Output ---
        
        xcal_u: N x ncp matrix of U scores (no scaling with singular values in S)
        xcal_t: T = US. N x ncp matrix of T scores (scaled with singular values in S)
        
        U with euclidean is equivalent to T=US with mahalanobis
        
        '''
        first_ncp = 0
        xx = self.get_xcal()
        Nin = self.Ncal
        xx_c = (xx - xx.mean(axis=0))
        ncp = self.ncp
        U,sval,Vt = np.linalg.svd(xx_c, full_matrices = False, compute_uv=True)
        Sval = np.zeros((U.shape[0], Vt.shape[0]))
        Sval[0:sval.shape[0], 0:sval.shape[0]] = np.diag(sval)
        xx_u = U[:,first_ncp:ncp]
        xx_t = U[:,first_ncp:ncp].dot(Sval[first_ncp:ncp, first_ncp:ncp])
        
        self.xcal_u = xx_u.copy()
        self.xcal_t = xx_t.copy()      
        
        
    def get_xcal_u(self):
        ''' Get copy of xcal_u data '''
        return self.xcal_u 
    
    def get_xcal_t(self):
        ''' Get copy of xcal_t data '''
        return self.xcal_t
    
    
    # ---------------------------- random sample --------------------------------
        
    def random_sample(self, Nout = 10):
        
        
        xx = self.get_xcal() 
        Nin = xx.shape[0]
        all_samples = np.arange(0, Nin)
        
        included = np.random.choice(all_samples, size=Nout, replace=False)
        
        sample_selected = np.zeros((Nin, 1))
        sample_selected[included,0] = 1

        return sample_selected.astype(int).flatten()

        
        
        
        
    # ------------------------------------------ kennard stone -----------------------------

    def kennard_stone(self, Nout=10, fixed_samples = None, use_pca_scores = True, distance_metric = "mahalanobis"):

        ''' 
        This algorithm corresponds to Kennard Stone CADEX alg. KENNARD. R. W. and STONE. L. (1969). Computer Aided Design of Experiments,Technometrics, 11, 137-148
        It enables the update of a current selected subset by entering fixed_samples as a 1-D array of 0's and 1's where 1 = part of current subset
        Nout is yet the total number of samples, i.e, current + to be selected in the update
        
        
        --- Input ---
        
        Nout: total number of sample selected, including fixed_samples
        fixed_samples: 1-D numpy array with 1's and 0's of shape (Nin,), where Nin is total number of samples available. 1 is specified for the initial fixed samples
        use_pca_scores: (bool) True (default) use PCA scores instead of original values
        distance_metric: "mahalanobis" (default). See https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.cdist.html for options
        
        --- Output ---
        
        sample_id: 1-D vector of 1's and 0,s, 1 corresponds to selected samples.
  

        
        '''
        
        
        if use_pca_scores:
            xx = self.get_xcal_t()
        else:
            xx = self.get_xcal()           
                
        
        
        Nin = xx.shape[0]
        K = xx.shape[1]
        sample_selected = np.zeros((Nin, 1))
        n_vector = list(range(Nin))

        xcal_in = xx.copy()
        
        
        # Initialize

        if fixed_samples is None or fixed_samples.flatten().sum()==0:
            iin = 0
            DD = distance.cdist(xcal_in, xcal_in.mean(axis=0).reshape((1,K)), metric = distance_metric)
            ID = DD.argmin()
            sample_selected[ID, 0] = 1
        else:
            iin = fixed_samples.sum()-1
            sample_selected = fixed_samples.copy().reshape((Nin,1))

        assert Nout >= sample_selected.flatten().sum(), "Nout must be bigger or equal the number of fixed_samples"
        
        DD_all = distance.cdist(xcal_in, xcal_in, metric = distance_metric)
        
        for ii in range(Nin):
            if sample_selected[ii,0] == 1:
                n_vector.remove(ii)
        

        while  iin < (Nout-1) and len(n_vector)>0:

            iin += 1
            DD = DD_all[sample_selected.flatten()==0,:][:,sample_selected.flatten()==1]            
            DD_row = DD.min(axis=1)
            max_DD = DD_row.max(axis=0)
            ID = DD_row.argmax(axis=0)
            sample_selected[n_vector[ID], 0] = 1
            n_vector.remove(n_vector[ID])
             
 
        return sample_selected.astype(int).flatten()


    
    
