

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib as mtply
from sklearn.cross_decomposition import PLSRegression
from sklearn.model_selection import GridSearchCV,cross_val_predict
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.linear_model import LinearRegression


def get_mod_performance(Ytar_test, Ytar_test_pred,chosen_lv,x_perf_t,y_perf_t,property_name,type_data, fig_width = 15,fig_height = 10):


 
    rmsep_t = np.sqrt(mean_squared_error(Ytar_test, Ytar_test_pred))
    r2p_t = np.power(np.corrcoef(Ytar_test.T, Ytar_test_pred.T)[0,1],2)


    # model performance


    fig, ax = plt.subplots(1,1, figsize = (fig_width,fig_height))

    test_title = "{:d} lv \nRMSEP: {:.2f} \n$r^2_p$: {:.2f}".format(chosen_lv, rmsep_t, r2p_t) 


    ax.plot(Ytar_test, Ytar_test_pred,'o',c = "#2A9EB3", markersize = 15)
    ax.plot([np.amin(Ytar_test),np.amax(Ytar_test)],[np.amin(Ytar_test),np.amax(Ytar_test)], c = "black")
    ax.set_xlabel("{} Observed".format(property_name))
    ax.set_ylabel("{} Predicted".format(property_name))
    ax.text(x = x_perf_t, y = y_perf_t, s=test_title)
    ax.set_title("{}".format(type_data))
    
    
    model_performance = {}
    model_performance["RMSEP"] = "{:.2f}".format(rmsep_t)
    model_performance["R2P"] = "{:.2f}".format(r2p_t)
 
    
    return model_performance, (fig, ax)



