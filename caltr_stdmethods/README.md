# Calibration transfer with linear transformations and standard samples

Applications of different chemometrics techniques for multivariate calibration

Methods are available in the Python library [pycaltransfer](https://pypi.org/project/pycaltransfer/)

- Direct Standardization (DS)
- Piecewise Direct Standardization (PDS)
- Spectral Space Transformation (SST)
- Slope and Bias Correction (SBC)

# Calibration transfer functions 

```python

from pycaltransfer import caltransfer as ct

F_ds,a_ds = ct.ds_pc_transfer_fit(Xsou, Xtar)
F_pds,a_pds = ct.pds_pls_transfer_fit(Xsou, Xtar, max_ncp = 1, ww = 1)
F_sst,a_sst = ct.sst(Xsou, Xtar, ncomp = 1)
Ytar_pred = Xtar.dot(B) + beta # existing calibration
slope, bias = ct.slope_bias_correction(Ysou, Ytar_pred)

```

# Performance of calibration transfer

Calibration model applied to source instrument and to target instrument with the different methods

![1](figures/source_test.png)
![2](figures/target_test.png)
![3](figures/target_test_ds.png)
![4](figures/target_test_pds.png)
![5](figures/target_test_sst.png)
![6](figures/target_test_sbc.png)

# Acknowledgements
As part of our doctoral research, we disseminate the available methodology for the chemometrics community at [MeBioS Biophotonics](https://www.biw.kuleuven.be/biosyst/mebios/biophotonics-group/biophotonics-staff)

Ph.D. research funded by [The Research Foundation – Flanders](https://www.fwo.be/) in Belgium


# References
See the information [pycaltransfer](https://pypi.org/project/pycaltransfer/)

